from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.ArticleListView.as_view(), name="top"),
    path('users/login/', auth_views.LoginView.as_view(template_name='olyful/users_login.html'), name='users_login'),
    path('users/logout/', auth_views.LogoutView.as_view(), name='users_logout'),
    path('shoppage/', views.shoppage, name='shoppage'),
    path('mypage/<int:pk>', views.mypage, name='mypage'),
    path('mypage/<int:pk>/edit', views.edit, name='edit'),
    path('traininfo/', views.traininfo, name='traininfo'),
    #path('', views.SearchResultView.as_view(), name='search'),
]
# MEDIA_ROOTを公開する（アクセス可能にする）
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
