from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from accounts.models import ShopInfo
from django.views import generic
from django.views.generic import ListView
from django.http import Http404, HttpResponse, JsonResponse #
from django.core import serializers
import json
import time
import olyful.traffic
from . import models
from django.contrib.auth import get_user_model

# Create your views here.
#def top(request):
#    """トップ画面"""
#    return render(request, 'olyful/top.html')

class ArticleListView(generic.ListView):
    #トップ画面
    model = ShopInfo
    template_name = 'olyful/top.html'


@login_required
def shoppage(request):
    shopinfo_list = get_user_model().objects.all()
    context = {'shopinfo_list' : shopinfo_list}
    return render(request, 'olyful/shoppage.html', context)


def mypage(request, pk):
    user = get_object_or_404(User, pk=pk)
    context = {
        'user': user,
    }
    return render(request, 'olyful/mypage.html', context)


def edit(request,pk):
    template_name = "olyful/edit.html"
    try:
        user = User.objects.get(pk=pk)
    except models.User.DoesNotExist:
        raise Http404
    if request.method == "POST":
        user.username = request.POST["username"]
        user.email = request.POST["email"]
        user.save()
        return redirect(mypage,pk)
    context = {"user": user}
    return render(request, template_name, context)

def traininfo(request):
    now = olyful.traffic.time
    trouble_info = olyful.traffic.trouble_info
    major_line = olyful.traffic.major_line
    context = {
        'time': now,
        'trouble_info': trouble_info,
        'major_line': major_line
    }
    return render(request, 'olyful/traininfo.html', context)
