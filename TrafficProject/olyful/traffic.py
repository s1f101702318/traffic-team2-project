import urllib.request
import requests
import bs4

def initialize():
    url = 'https://transit.yahoo.co.jp/traininfo/area/4/'
    s = requests.Session()
    r = s.get(url)
    bs = bs4.BeautifulSoup(r.text, 'html.parser')
    return bs

def time():
    bs = initialize()
    result = bs.select('.labelLarge')[0].select('.subText')[0].text
    
    return result


def trouble_info():
    bs = initialize()
    trouble = bs.select('.elmTblLstLine')[0]
    td_all = trouble.find_all('td')
    count = 0 # 路線、状況、詳細の3つを文字列に格納し、一行ごとに表示
    result = ''
    for td in td_all:
        count += 1
        result += td.text
        result += '\t'
        if count % 3 == 0:
            result += '\n'
    if result == '':
        result += '現在事故・遅延情報はありません'
    return result

def major_line():
    bs = initialize()
    major = bs.select('.navAreaMajorLineCommon')[0]
    line = major.select('.line')[0]
    line_a_all = line.find_all('a')
    result = ''
    for i in range(len(line_a_all)):
        result += line_a_all[i].text
        result += '|'
    return result