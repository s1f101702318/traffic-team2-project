from django.db import models
from django.conf import settings
from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import ResizeToFill
from django.core.validators import RegexValidator

class Tag(models.Model):
    name = models.CharField(max_length=128, unique=True)

    def __str__(self):
        return self.name

VENUES_CHOICES = [
    (1, 'オリンピックスタジアム'),
    (2, '東京体育館'),
    (3, '国立代々木競技場'),
    (4, '日本武道館'),
    (5, '東京国際フォーラム'),
    (6, '国技館'),
    (7, '馬事公苑'),
    (8, '武蔵野の森総合スポーツプラザ'),
    (9, '東京スタジアム'),
    (10, '武蔵野の森公園'),
    (11, '有明アリーナ'),
    (12, '有明体操競技場'),
    (13, '有明アーバンスポーツパーク'),
    (14, '有明テニスの森'),
    (15, 'お台場海浜公園'),
    (16, '潮風公園'),
    (17, '青海アーバンスポーツパーク'),
    (18, '大井ホッケー競技場'),
    (19, '海の森クロスカントリーコース'),
    (20, '海の森水上競技場'),
    (21, 'カヌー・スラロームセンター'),
    (22, '夢の島公園アーチェリー場'),
    (23, '東京アクアティクスセンター'),
    (24, '東京辰巳国際水泳場'),
]

class ShopInfo(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    shop_name = models.CharField('店舗名', max_length=40)
    tag = models.ManyToManyField(Tag)
    #venues = models.CharField('最寄りの会場', max_length=60, default="")
    #station = models.CharField('最寄りの駅', max_length=60, default="")
    address = models.CharField('住所', max_length=60, default="")
    phone_regex = RegexValidator(regex=r'^[0-9]+$', message = ("Tell Number must be entered in the format: '09012345678'. Up to 15 digits allowed."))
    phone = models.CharField(validators=[phone_regex], max_length=15, verbose_name='電話番号(半角/ハイフンなし)', default="")
    menu = models.TextField('メニュー', default="")
    special = models.TextField('利用者への特典(クーポン等)', blank=True, null=True)
    shop_image = models.ImageField('店の写真', upload_to='media', default="", blank=True, null=True)

    # shop_imageをリサイズ
    top = ImageSpecField(source="shop_image", processors=[ResizeToFill(700, 500)], format='JPEG')
    thumbnail = ImageSpecField(source='shop_image', processors=[ResizeToFill(250,250)], format="JPEG", options={'quality': 60})
    food = ImageSpecField(source='shop_image', processors=[ResizeToFill(600, 400)], format="JPEG", options={'quality': 75} )

    class Meta:
        permissions = (
            ('create_shoppage', 'Can create shoppage'),
        )

    def __str__(self):
        return self.shop_name
