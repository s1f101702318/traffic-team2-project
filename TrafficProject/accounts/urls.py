from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

# set the application namespace
# https://docs.djangoproject.com/en/2.0/intro/tutorial03/
app_name = 'accounts'

urlpatterns = [
    path('login/', auth_views.LoginView.as_view(template_name="registration/login.html"), name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page="top"), name='logout'),
    path('shop/signup/', views.register_user, name='signup'),
    path('app/signup/', views.register_app_user, name='app_signup'),
    path("create/", views.ArticleCreateView.as_view(), name="create"),
    path("<int:pk>/detail/", views.ArticleDetailView.as_view(), name="detail"),
    #path("update/<int:pk>/", views.ShopUpdate.as_view(), name="update"),
    #path("delete/", views.ArticleDeleteView.as_view(), name="delete"),
    path("tag/create/", views.TagCreateView.as_view(), name="create_tag"),
    path("search/", views.index, name="search"),
]
