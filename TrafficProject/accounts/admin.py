from django.contrib import admin
from django.contrib.auth.models import Group,User
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from .models import ShopInfo,Tag

admin.site.unregister(Group)
admin.site.register(ShopInfo)
admin.site.register(Tag)
