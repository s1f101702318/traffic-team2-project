from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.contrib.auth.forms import AuthenticationForm
from django.forms import ModelForm #
from .models import ShopInfo #

class UserCreateForm(UserCreationForm):
    #入力を必須にするため、required=Trueで上書き
    email = forms.EmailField(required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['class'] = 'form-control'
        self.fields['email'].widget.attrs['class'] = 'form-control'
        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['class'] = 'form-control'

    class Meta:
       model = User
       fields = (
           "username", "email", "password1", "password2"
       )

    def clean_email(self):
        email = self.cleaned_data["email"]
        try:
            validate_email(email)
        except ValidationError:
            raise ValidationError("正しいメールアドレスを指定して下さい。")

        try:
            User.objects.get(email=email)
        except User.DoesNotExist:
            return email
        else:
            raise ValidationError("このメールアドレスは既に使用されています。別のメールアドレスを指定してください")



"""検索フォーム"""
class BlogForm(ModelForm):
    class Meta:
        model = ShopInfo
        fields = ['shop_name', 'tag']

class BlogSearchForm(forms.Form):
    shop_name = forms.CharField(label='店舗名', required=False)
    tag = forms.CharField(label='タグ', required=False)

BlogSearchFormSet = forms.formset_factory(BlogSearchForm, extra=1)


"""店舗情報更新フォーム"""
class ShopUpdateForm(forms.ModelForm):
    class Meta:
        model = ShopInfo
        fields = ('shop_name', 'tag', 'address', 'phone', 'menu', 'special', 'shop_image')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control'
