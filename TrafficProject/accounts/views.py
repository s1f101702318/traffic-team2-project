from django.shortcuts import render, get_object_or_404, redirect
from .forms import UserCreateForm
from django.contrib.auth import login
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.http import HttpResponse
from django.views import generic
from .models import ShopInfo,Tag

from django.db.models import Q
from django.views.decorators.http import require_POST
from .forms import BlogForm, BlogSearchFormSet, ShopUpdateForm


def register_user(request):
    """ユーザー登録（店用）"""
    user_form = UserCreateForm(request.POST or None)
    if request.method == "POST" and user_form.is_valid():
        # Userモデルの処理。ログインできるようis_activeをTrueにし保存
        user = user_form.save(commit=False)
        user.is_active = True
        user.save()
        login(request, user, backend="django.contrib.auth.backends.ModelBackend")
        return redirect("accounts:create")

    context = { "user_form": user_form, }
    return render(request, 'accounts/signup.html', context)


class ArticleCreateView(generic.edit.CreateView):
    """店の情報　登録"""
    model = ShopInfo
    fields = ["shop_name","tag","address","phone","menu","special","shop_image"]
    #fields = ["shop_name","tag","venues","station","address","phone","menu","special","shop_image"]
    template_name = "accounts/create.html"
    success_url = reverse_lazy('top')  #POSTが正しく行われた際に飛ばすURL


class ArticleDetailView(generic.DetailView):
    """店の情報を見る"""
    model = ShopInfo
    #shopinfo = Shop.objects.get(pk=pk)
    template_name = "accounts/detail.html"


class TagCreateView(generic.edit.CreateView):
    """タグの作成"""
    model = Tag
    fields = ["name"]
    template_name = "accounts/create-tag.html"
    success_url = reverse_lazy('accounts:create')  #POSTが正しく行われた際に飛ばすURL


def register_app_user(request):
    """ユーザー登録"""
    user_form = UserCreateForm(request.POST or None)
    if request.method == "POST" and user_form.is_valid():

        # Userモデルの処理。ログインできるようis_activeをTrueにし保存
        user = user_form.save(commit=False)
        user.is_active = True
        user.save()

        login(request, user, backend="django.contrib.auth.backends.ModelBackend")

        return redirect("top")

    context = {
        "user_form": user_form,
    }
    return render(request, 'accounts/app_signup.html', context)


def index(request):
    """検索"""
    shop_list = ShopInfo.objects.all()
    formset = BlogSearchFormSet(request.POST or None)
    if request.method == 'POST':
        formset.is_valid()

        queries = []

        for form in formset:
            q_kwargs = {}
            shop_name = form.cleaned_data.get('shop_name')
            if shop_name:
                q_kwargs['shop_name'] = shop_name

            tag = form.cleaned_data.get('tag')
            if tag:
                q_kwargs['tag'] = tag

            if q_kwargs:
                q = Q(**q_kwargs)
                queries.append(q)

        if queries:
            base_query = queries.pop()
            for query in queries:
                base_query |= query
            shop_list = shop_list.filter(base_query)

    return render(request, 'olyful/search.html', {'shop_list': shop_list, 'formset': formset})

"""
#@login_required
class ShopUpdate(generic.UpdateView):
    #情報更新（未）
    model = ShopInfo
    form_class = ShopUpdateForm
    #template_name = 'accounts/update.html'
    template_name = 'update.html'
    success_url = "/"
"""
"""
#@login_required
class ArticleDeleteView(generic.edit.DeleteView):
    #店のページ削除（未）
    model = ShopInfo
    template_name = "accounts/delete.html"
    success_url = reverse_lazy('top')  #POSTが正しく行われた際に飛ばすURL"""
